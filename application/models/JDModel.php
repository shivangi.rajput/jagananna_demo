<?php

defined('BASEPATH') or exit('No direct script access allowed');

class JDModel extends CI_Model{
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }

    public $secret_key = '847EJDALQIS2PE3UDKA7128409EJA';
    /*******
     * Fetch data from any table based on different conditions
     *
     * @access	public
     * @param	string
     * @param	string
     * @param	array
     * @return	bool
     */
    public function fetch_data($table, $fields = '*', $conditions = array(), $returnRow = false, $distinct = false){

        //Preparing query
        $this->db->select($fields);
        $this->db->from($table);

        //If there are conditions
        if (count($conditions) > 0) {
            $this->condition_handler($conditions);
        }

        if ($distinct) {
            $this->db->distinct();
        }

        $query = $this->db->get();
        // print_r($this->db->last_query());
        // exit;
        // Return
        if (FALSE != $query && $query->num_rows() > 0) {

            // $this->totalrows = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
            return $returnRow ? $query->row_array() : $query->result_array();
        } else {
            return NULL;
        }
    }

    /**
     * Count all records
     *
     * @access	public
     * @param	string
     * @return	array
     */
    public function fetch_count($table, $conditions = array())
    {
        $this->db->from($table);
        //If there are conditions
        if (count($conditions) > 0) {
            $this->condition_handler($conditions);
        }
        return $this->db->count_all_results();
    }

    /**
     * Handle different conditions of query
     *
     * @access	public
     * @param	array
     * @return	bool
     */
    private function condition_handler($conditions)
    {
        //Inner Join
        if (array_key_exists('inner_join', $conditions)) {

            //Iterate all where's
            foreach ($conditions['inner_join'] as $key => $val) {
                $this->db->join($key, $val, 'inner');
            }
        }
        // print_r($conditions);
        // die;
        //Left Join
        if (array_key_exists('left_join', $conditions)) {

            //Iterate all where's
            foreach ($conditions['left_join'] as $key => $val) {
                $this->db->join($key, $val, 'left');
            }
        }

        //Where
        if (array_key_exists('where', $conditions)) {

            //Iterate all where's
            if (is_array($conditions['where'])) {
                foreach ($conditions['where'] as $key => $val) {
                    $this->db->where($key, $val);
                }
            } else {
                $this->db->where($conditions['where']);
            }
        }

        //Where OR
        if (array_key_exists('or_where', $conditions)) {

            //Iterate all where or's

            if (is_array($conditions['or_where'])) {
                foreach ($conditions['or_where'] as $key => $val) {
                    $this->db->or_where($key, $val);
                }
            } else {
                $this->db->or_where($conditions['or_where']);
            }
        }

        //Where In
        if (array_key_exists('where_in', $conditions)) {

            //Iterate all where in's
            foreach ($conditions['where_in'] as $key => $val) {
                $this->db->where_in($key, $val);
            }
        }

        //Where Not In
        if (array_key_exists('where_not_in', $conditions)) {

            //Iterate all where in's
            foreach ($conditions['where_not_in'] as $key => $val) {
                $this->db->where_not_in($key, $val);
            }
        }

        //Having
        if (array_key_exists('having', $conditions)) {
            $this->db->having($conditions['having']);
        }

        //Group By
        if (array_key_exists('group_by', $conditions)) {
            $this->db->group_by($conditions['group_by']);
        }

        //Order By
        if (array_key_exists('order_by', $conditions)) {

            //Iterate all order by's
            foreach ($conditions['order_by'] as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }

        //Order By
        if (array_key_exists('like', $conditions)) {

            if (is_array($conditions['like'])) {
                //Iterate all likes
                foreach ($conditions['like'] as $key => $val) {
                    $this->db->like($key, $val);
                }
            } else {
                $this->db->where($conditions['like']);
            }
        }

        //Limit
        if (array_key_exists('limit', $conditions)) {
            //If offset is there too?
            if (count($conditions['limit']) == 1) {
                $this->db->limit($conditions['limit'][0]);
            } else {
                $this->db->limit($conditions['limit'][0], $conditions['limit'][1]);
            }
        }
    }

    /**
     * Insert data in DB
     *
     * @access	public
     * @param	string
     * @param	array
     * @param	string
     * @return	string
     */
    public function insert_single($table, $data = array())
    {
        //Check if any data to insert
        if (count($data) < 1) {
            return false;
        }

        $this->db->insert($table, $data);
        //echo $this->db->last_query();

        return $this->db->insert_id();
    }

    public function insert_multiple($table, $data = array())
    {
        //Check if any data to insert
        if (count($data) < 1) {
            return false;
        }
        $this->db->insert_batch($table, $data);
        return true;
    }



    /**
     * Insert batch data
     *
     * @access	public
     * @param	string
     * @param	array
     * @param	array
     * @param	bool
     * @return	bool
     */
    public function insert_batch($table, $defaultArray, $dynamicArray = array(), $updatedTime = false)
    {
        //Check if default array has values
        if (count($dynamicArray) < 1) {
            return false;
        }

        //If updatedTime is true
        if ($updatedTime) {
            $defaultArray['UpdatedTime'] = time();
        }

        //Iterate it
        foreach ($dynamicArray as $val) {
            $updates[] = array_merge($defaultArray, $val);
        }
        return $this->db->insert_batch($table, $updates);
    }

    /**
     * Update details in DB
     *
     * @access	public
     * @param	string
     * @param	array
     * @param	array
     * @return	string
     */
    public function update_single($table, $updates, $conditions = array())
    {
        //If there are conditions
        if (count($conditions) > 0) {
            $this->condition_handler($conditions);
        }
        return $this->db->update($table, $updates);
    }

    /**
     * Duplicate records in DB
     *
     * @access	public
     * @param	string
     * @return	string
     */
    function duplicate_record($table, $primary_key_field, $primary_key_val)
    {
        /* CREATE SELECT QUERY */
        $this->db->where($primary_key_field, $primary_key_val);
        $query = $this->db->get($table);
        foreach ($query->result() as $row) {
            foreach ($row as $key => $val) {
                if ($key != $primary_key_field && $key != 'unique_id') {
                    //Below code can be used instead of passing a data array directly to the insert or update functions  
                    $this->db->set($key, $val);
                } //endif 
            } //endforeach 
        } //endforeach 

        //insert the new record into table 
        $this->db->insert($table);
        return $this->db->insert_id();
    }



    function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) {
            return $min;
        }
        // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }

    function getToken($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max - 1)];
        }
        return $token;
    }

    public function getClientIp($defaultIP = '127.0.0.1')
    {
        $ipaddr = null;
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ipaddr = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipaddr = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ipaddr = $_SERVER['REMOTE_ADDR'];
        }
        $ipaddr = trim($ipaddr);
        if ($ipaddr == '::1') {
            $ipaddr = $defaultIP;
        }
        return $ipaddr;
    }

    public function checkToken($token)
    {
        $this->db->select('*');
        $this->db->from('users_user AS u');
        $this->db->join('users_token AS ut', 'u.id = ut.user_id', false);
        $this->db->where('token', $token);
        $query = $this->db->get();
        if (FALSE != $query && $query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return NULL;
        }
    }

    // send mail 
    public function sendmail($message, $to, $subject)
    {
        $this->load->library('email');
        $config['protocol']    = 'smtp';
        $config['smtp_host']    = 'ipac.splitmx.com';
        $config['smtp_port']    = '587';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = 'pta@indianpac.com';
        $config['smtp_pass']    = 'AXbu*aI!{G&f_%WKIg';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not
        // $this->email->set_header('MIME-Version', '1.0; charset=utf-8'); //must add this line
        $this->email->set_header('Content-type', 'text/html'); //must add this line

        $this->email->initialize($config);
        $this->email->from('info@indianpac.com', 'FSCS Dashboard');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }
    //end send mail

    // end percentage count function 
    public function customdateformat($chkdt)
    {
        $month = substr($chkdt, 4, 3);
        if ($month == 'Jan') {
            $month = '01';
        } else if ($month == 'Feb') {
            $month = '02';
        } else if ($month == 'Mar') {
            $month = '03';
        } else if ($month == 'Apr') {
            $month = '04';
        } else if ($month == 'May') {
            $month = '05';
        } else if ($month == 'Jun') {
            $month = '06';
        } else if ($month == 'Jul') {
            $month = '07';
        } else if ($month == 'Aug') {
            $month = '08';
        } else if ($month == 'Sep') {
            $month = '09';
        } else if ($month == 'Oct') {
            $month = '10';
        } else if ($month == 'Nov') {
            $month = '11';
        } else if ($month == 'Dec') {
            $month = '12';
        } else {
            return $chkdt;
        }

        $date = substr($chkdt, 7, 3);
        $year = substr($chkdt, 10, 5);
        return date("Y-m-d", mktime(0, 0, 0, $month, $date, $year));
    }

    function encrypt_decrypt($action, $string, $secret_key = "")
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_iv = 'ipac@12361'; // change this to one more secure
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {

            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

    function getLocationAddress($lat, $long)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&key=AIzaSyAqOuo69mntSw3f1zPM_bGEBmN6t_tlyMw&sensor=false");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);

        // convert response
        $output = json_decode($output);
        // handle error; error output
        if (curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200) {
            $address['msg'] = $output->error_message;
            $address['code'] = 202;
        } else {
            $address['msg'] = $output->results[0]->formatted_address;
            $address['code'] = 200;
        }
        return $address;
    }
}
