<?php

require APPPATH . 'libraries/REST_Controller.php';

class Api extends REST_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->library(array("form_validation"));
        $this->load->helper("security");
        //$this->load->helper('s3');
        $this->load->database();
        date_default_timezone_set('Asia/Kolkata');
    }

    public function get_districts_get(){
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $table_name = 'ap_district_ac_mapping';
            $data       = $this->JDModel->fetch_data($table_name, 'district', [], false, true);

            $response   = [];
            foreach ($data as $key => $value) {
                array_push($response, $value['district']);
            }
            $this->response(array(
                "valid" => true,
                "status" => 'OK',
                "result" => array(
                    "data"     => !empty($response) ? $response : array()
                )
            ), REST_Controller::HTTP_OK);
        }
    }

    public function get_acs_post(){
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $district_name  = trim($this->security->xss_clean($this->input->post("district_name")));
            if(!empty($district_name)){
                $table_name     = 'ap_district_ac_mapping';
                $cond['where']  = array('district' => $district_name);
                $data           = $this->JDModel->fetch_data($table_name, 'ac', $cond, false, true);
            }

            if(!empty($data)){
                foreach($data as $key=>$val){
                    $result[]=$val['ac'];
                }
            }else{
                $result =array();
            }
            $this->response(array(
                "valid" => true,
                "status" => 'OK',
                "result" => array(
                    "data"     => $result
                )
            ), REST_Controller::HTTP_OK);
        }
    }

    public function get_party_get(){
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $data           = ["BJP", "INC", "JSP", "TDP", "YSRCP"];
            array_push($data, "Prefer not to say");

            $this->response(array(
                "valid"     => true,
                "status"    => 'OK',
                "result"    => array(
                    "data"     => !empty($data) ? $data : array()
                )
            ), REST_Controller::HTTP_OK);
        }
    }

    public function get_occupation_get(){        
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            
            $data           = ["Student", "Service", "Self Employed"];
           
            array_push($data, "Other");

            print_r($data);die();

            $this->response(array(
                "valid"     => true,
                "status"    => 'OK',
                "result"    => array(
                    "data"     => !empty($data) ? $data : array()
                )
            ), REST_Controller::HTTP_OK);
        }
    }

    public function get_social_media_platform_get(){
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $data           = ["Facebook", "Twitter", "Instagram", "Youtube"];

            $this->response(array(
                "valid"     => true,
                "status"    => 'OK',
                "result"    => array(
                    "data"     => !empty($data) ? $data : array()
                )
            ), REST_Controller::HTTP_OK);
        }
    }

    public function get_editing_apps_get(){
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $data           = ["Photoshop", "Premiere Pro", "Canva", "Inshots", "Kinemaster", "Power Director", "Pixel Lab"];

            $this->response(array
            
            (
                "valid"     => true,
                "status"    => 'OK',
                "result"    => array(
                    "data"     => !empty($data) ? $data : array()
                )
            ), REST_Controller::HTTP_OK);
        }
    }

    public function get_profile_post(){
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            // print_r("fff");die();
            $id             = trim($this->security->xss_clean($this->input->post("uid")));
            $secret_key     = $this->JDModel->secret_key;
            $id             = $this->JDModel->encrypt_decrypt('decrypt', $id, $secret_key);

            $table_name     = 'ap_membership_registration';
            $cond['where']  = array('id' => $id);
            $data           = $this->JDModel->fetch_data($table_name, '*', $cond, true);
            $is_exist       = false;
            $final_res      = array();
            if (!empty($data)) {
                $is_exist   = true;

                $final_res['membership_id']     = $data['unique_id'];
                $final_res['name']              = $data['fullName'];
                $final_res['age']               = $data['age'];
                $final_res['district']          = $data['district'];
                $final_res['ac']                = $data['ac'];
                $final_res['registration_date'] = date("d/m/Y", strtotime($data['registration_date']));
            }

            $this->response(array(
                "valid" => true,
                "status" => 'OK',
                "result" => array(
                    "data"     => $final_res,
                    "is_exist" => $is_exist
                )
            ), REST_Controller::HTTP_OK);
        }
    }

    public function get_referral_districts_get(){
        if ($this->input->server('REQUEST_METHOD') == 'GET') {
            $table_name = 'ap_referral_district_ac_mapping';
            $data       = $this->JDModel->fetch_data($table_name, 'district,district_te', [], false, true);

            $response   = [];
            foreach ($data as $key => $value) {
                $response[] = array('district' => $value['district'], 'district_te' => $value['district_te']);
            }
            $this->response(array(
                "valid" => true,
                "status" => 'OK',
                "result" => array(
                    "data"     => !empty($response) ? $response : array()
                )
            ), REST_Controller::HTTP_OK);
        }
    }

    public function get_referral_acs_post(){
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $district_name  = trim($this->security->xss_clean($this->input->post("district_name")));

            if(!empty($district_name)){
                $table_name = 'ap_referral_district_ac_mapping';
                $cond['where'] = array('district' => $district_name);
                $data = $this->JDModel->fetch_data($table_name, 'ac_te', $cond, false, true);
            }
            if(!empty($data)){
                foreach($data as $key=>$val){
                    $result[]=$val['ac'];
                }
            }
            else{
                $result=array();
            }
            $this->response(array(
                "valid" => true,
                "status" => 'OK',
                "result" => array(
                    "data"     => !empty($response) ? $response : array()
                )
            ), REST_Controller::HTTP_OK);
        }
    }

    public function membership_registration_post(){
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $param                          = $this->input->post();
            $postData                       = [];
            foreach ($param as $k => $v) {
                $postData[$k]               = trim($this->security->xss_clean($v));
            }
            $postData['source']             = $postData['source'] != 'undefined' && $postData['source'] != 'null' ? $postData['source'] : null;
            $phoneNumber                    = $postData['phoneNumber'];
            $emailID                        = $postData['emailID'];

            $postData['registration_date']  = date("Y-m-d H:i:s");
            $secret_key                     = $this->JDModel->secret_key;
            $table_name                     = 'ap_membership_registration';

            $whereUser                      = array('where' => array('emailID' => $emailID));
            $emailExist                     = $this->JDModel->fetch_count($table_name, $whereUser);
            $whereUser                      = array('where' => array('phoneNumber' => $phoneNumber));
            $phoneNoExist                   = $this->JDModel->fetch_count($table_name, $whereUser);
            if ($emailExist > 0) {
                $this->response(array(
                    "valid"                 => true,
                    "status"                => 'NOK',
                    "result"                => array("message" => "Email ID already exist")
                ), REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
            } else if ($phoneNoExist > 0) {
                $this->response(array(
                    "valid"                 => true,
                    "status"                => 'NOK',
                    "result"                => array("message" => "Phone Number already exist")
                ), REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
            } else {
                $id                             = $this->JDModel->insert_single($table_name, $postData);
                $cond['where']                  = array('id' => $id);
                $unique_id                      = 'SMW' . (11010 + $id);
                $updateData                     = array('unique_id' => $unique_id);
                $updated_member                 = $this->JDModel->update_single($table_name, $updateData, $cond);
                $uid                            = $this->JDModel->encrypt_decrypt('encrypt', $id, $secret_key);

                $this->response(array(
                    "valid"                 => true,
                    "status"                => 'OK',
                    "result"                => array("uid" => $uid)
                ), REST_Controller::HTTP_OK);
            }
        }
    }

    public function get_leaderboard_get(){
        if ($this->input->server('REQUEST_METHOD') == 'GET') {

            $table_name = "ap_referral_registration as ref";
            $fields                 = 'ref.fullName,ref.ac,count(friend.id) as total_referrals';
            $cond['inner_join']     = array('ap_referrals AS friend' => "ref.id = friend.user_id");
            $cond['order_by']       = array('total_referrals' => 'DESC');
            $cond['group_by']       = 'ref.id';
            //$cond['limit']          = 10;
            
            $data                   = $this->JDModel->fetch_data($table_name, $fields, $cond, false, true);
            
            $this->response(array(
                "valid" => true,
                "status" => 'OK',
                "result" => array(
                    "data"  => !empty($data) ? $data : array()
                )
            ), REST_Controller::HTTP_OK);
        }
    }

    public function referral_registration_post(){
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $param                          = $this->input->post();
            $postData                       = [];
            foreach ($param as $k => $v) {
                if ($k !== 'referralArray' && $k !== 'lang') {
                    $postData[$k]               = trim($this->security->xss_clean($v));
                }
            }
            $postData['source']             = $postData['source'] != 'undefined' && $postData['source'] != 'null' ? $postData['source'] : null;
            $mobileNumber                   = $postData['mobileNumber'];
            $email                          = $postData['email'];

            $postData['x']  = date("Y-m-d H:i:s");
            $table_name                     = 'ap_referral_registration';

            $whereUser                      = array('where' => array('email' => $email));
            $emailExist                     = $this->JDModel->fetch_count($table_name, $whereUser);
            $whereUser                      = array('where' => array('mobileNumber' => $mobileNumber));
            $phoneNoExist                   = $this->JDModel->fetch_count($table_name, $whereUser);
            if ($emailExist > 0) {
                $this->response(array(
                    "valid"                 => true,
                    "status"                => 'NOK',
                    "result"                => array("message" => "Email ID already exist")
                ), REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
            } else if ($phoneNoExist > 0) {
                $this->response(array(
                    "valid"                 => true,
                    "status"                => 'NOK',
                    "result"                => array("message" => "Phone Number already exist")
                ), REST_Controller::HTTP_UNPROCESSABLE_ENTITY);
            } else {
                $id                             = $this->JDModel->insert_single($table_name, $postData);
                $cond['where']                  = array('id' => $id);
                $unique_id                      = 'SMREF' . (11010 + $id);
                $updateData                     = array('unique_id' => $unique_id);
                $updated_member                 = $this->JDModel->update_single($table_name, $updateData, $cond);
                $referralArray                  = trim($this->security->xss_clean($this->input->post("referralArray")));
                $referralArray_decode           = json_decode($referralArray);
                if (!empty($referralArray_decode) && count($referralArray_decode) > 0) {
                    foreach ($referralArray_decode as $ref) {
                        if ($ref->referralName != '' && $ref->referralWhatsppNumber != '') {
                            $referralData[] = array(
                                'user_id'               => $id,
                                'friendName'            => $ref->referralName,
                                'friendWhatsppNumber'   => $ref->referralWhatsppNumber
                            );
                        }
                    }

                    $tblPOC = 'ap_referrals';
                    $this->JDModel->insert_multiple($tblPOC, $referralData);
                }

                $msg = $this->input->post('lang') == 'en' ? 'You have successfully referred your friends to be Jagananna social media warriors and we will be reaching out to them soon.' : 'జగనన్న సోషల్ మీడియా టీంలోకి స్నేహితులను ఆహ్వానించిన అందరినీ త్వరలో సంప్రదించడం జరుగుతుంది.';
                $this->response(array(
                    "valid"                 => true,
                    "status"                => 'OK',
                    "result"                => array("message" => $msg)
                ), REST_Controller::HTTP_OK);
            }
        }
    }

}
?>